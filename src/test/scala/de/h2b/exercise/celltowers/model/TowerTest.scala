/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.model

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class TowerTest extends FunSuite {

  private val tower1 = Tower(262, 13.28527, 52.521711, None, true)
  private val tower2 = Tower(262, 13.276605, 52.525348, Some(5714), true)
  private val tower3 = Tower(262, 13.284916, 52.523771, Some(6278), false)

  test("equality") {
    val t1a = Tower(262, 13.28527, 52.521711, None, true)
    val t1b = Tower(262, 13.28527, 52.521711, None, true)
    val t2 = Tower(262, 13.284916, 52.523771, Some(6278), false)
    assert(t1a===t1b)
    assert(t1a!==t2)
  }

  test("to string") {
	  assertResult("262,13.28527,52.521711,,1")(tower1.toString)
	  assertResult("262,13.276605,52.525348,5714,1")(tower2.toString)
	  assertResult("262,13.284916,52.523771,6278,0")(tower3.toString)
  }

  test("from string") {
	  assertResult(tower1)(Tower(tower1.toString))
	  assertResult(tower2)(Tower(tower2.toString))
	  assertResult(tower3)(Tower(tower3.toString))
  }

  test("is covering") {
    assert(!tower1.isCovering(52.521711, 3.28527))
    assert(tower2.isCovering(52.525348, 13.276605))
    assert(tower2.isCovering(52.5, 13.3))
    assert(!tower2.isCovering(52.5, 13.2))
  }

}