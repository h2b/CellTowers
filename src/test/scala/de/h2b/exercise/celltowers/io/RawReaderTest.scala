/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.io

import scala.Vector
import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.exercise.celltowers.model.Tower

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class RawReaderTest extends FunSuite {

  private val testSource = """radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal
UMTS,262,2,801,86355,,13.28527,52.521711,,7,1,1282569574,1300175362,-91
GSM,262,2,801,1795,,,52.525348,5714,9,1,1282569574,1300175362,-87
GSM,262,2,801,1794,,13.284916,52.523771,6278,13,0,1282569574,1300816026,-91
"""

  private val expected = Vector(
      Some(Tower(262, 13.28527, 52.521711, None, true)),
      None,
      Some(Tower(262, 13.284916, 52.523771, Some(6278), false)))

  test("iterating test source") {
    val source = Source.fromString(testSource)
    val reader = RawReader(source)
    var i = 0
    for (tower ← reader) {
      assertResult(expected(i), s"for index $i")(tower)
      i += 1
    }
    assertResult(3)(i)
    assertResult(1)(reader.errorLineCount)
    source.close()
  }

  test("providing empty source") {
    val source = Source.fromString("")
    for (tower ← RawReader(source)) fail("executed by empty iterator")
    source.close()
  }

  test("providing null source") {
    intercept[IllegalArgumentException](RawReader(null).iterator)
  }

}