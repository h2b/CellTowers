/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.io

import java.io.File

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.exercise.celltowers.model.Tower

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class WriterTest extends FunSuite {

  private val testFilenamePrefix = "writertest"
  private val testFilenameSuffix = ".out.txt"

  private val tower1 = Tower(262, 13.28527, 52.521711, None, true)
  private val tower2 = Tower(262, 13.276605, 52.525348, Some(5714), true)
  private val tower3 = Tower(262, 13.284916, 52.523771, Some(6278), false)

  private val expected = """262,13.28527,52.521711,,1
262,13.276605,52.525348,5714,1
262,13.284916,52.523771,6278,0
"""

  test("writing tower data to file") ({
    val file = File.createTempFile(testFilenamePrefix, testFilenameSuffix)
    val writer = Writer.toFile(file)
    writer.write(tower1)
    writer.write(tower2)
    writer.write(tower3)
    writer.close()
    val source = Source.fromFile(file)
    assertResult(expected)(source.mkString)
    file.delete()
  })

  test("providing null file") {
    intercept[IllegalArgumentException](Writer.toFile(null))
  }

}