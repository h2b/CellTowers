/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.io

import scala.Vector
import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.exercise.celltowers.model.Tower

@RunWith(classOf[JUnitRunner])
class LoaderTest extends FunSuite {

  private val testSource = """602,31.237097,30.072728,,1
602,31.401373,30.120031,*ERROR*,1
602,31.405286,30.122675,373,0
"""

  private val expected = Vector(
      Some(Tower(602,31.237097,30.072728,None,true)),
      None,
      Some(Tower(602,31.405286,30.122675,Some(373),false)))

  test("iterating test source") {
    val source = Source.fromString(testSource)
    val loader = Loader.fromSource(source)
    var i = 0
    for (tower ← loader) {
      assertResult(expected(i), s"for index $i")(tower)
      i += 1
    }
    assertResult(3)(i)
    assertResult(1)(loader.errorCount)
    loader.close()
  }

  test("providing empty source") {
    val source = Source.fromString("")
    val loader = Loader.fromSource(source)
    for (tower ← loader) fail("executed by empty iterator")
    loader.close()
  }

  test("providing null source") {
    intercept[IllegalArgumentException](Loader.fromSource(null).iterator)
  }

}