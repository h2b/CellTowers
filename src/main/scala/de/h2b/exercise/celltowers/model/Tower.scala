/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.model

import scala.math._

/**
 * Represents a cell tower with mobile country code, longitude and latitude
 * coordinates, signal range and a Boolean stating if these data can change or
 * not.
 *
 * @author h2b
 */
class Tower private (val mcc: Int, val lon: Double, val lat: Double,
    val range: Option[Int], val changeable: Boolean) extends Equals {

  import Tower._

  def canEqual(other: Any) = other.isInstanceOf[Tower]

  override def equals(other: Any) = other match {
    case that: Tower => that.canEqual(this) &&
  		  mcc == that.mcc && lon == that.lon && lat == that.lat &&
  		  range == that.range && changeable == that.changeable
    case _ => false
  }

  override def hashCode() = {
      val prime = 41
      prime * (prime * (prime * (prime * (prime + mcc.hashCode) +
          lon.hashCode) + lat.hashCode) + range.hashCode) + changeable.hashCode
    }

  override def toString = {
    val builder = new StringBuilder()
    builder ++= mcc.toString; builder += FieldSeparator
    builder ++= lon.toString; builder += FieldSeparator
    builder ++= lat.toString; builder += FieldSeparator
    builder ++= range.getOrElse("").toString; builder += FieldSeparator
    builder += (if (changeable) TrueChar else  FalseChar)
    builder.result()
  }

  /**
   * Checks if the specified coordinates are within the range of this tower,
   * measured by the Euclidean distance between the two points defined by the
   * tower's coordinates and the specified coordinates.
   *
   * Note that this neither takes into account the earth curvature nor any
   * obstacles that both might shadow the signal.
   *
   * @note If the range of this tower is not defined, a default range is
   * applied.
   *
   * @see [[http://www.kompf.de/gps/distcalc.html]]
   *
   * @param lat
   * @param lon
   * @return `true` if this tower covers `(lat, lon)` or `false` else
   */
  def isCovering (lat: Double, lon: Double): Boolean = {
    val radius = range.getOrElse(DefaultRange).toDouble
	  val midlat = (this.lat+lat)/2
	  val dx = (this.lon-lon)*DistancePerDegree*cos(midlat*DegreeInRadian)
	  val dy = (this.lat-lat)*DistancePerDegree
	  dx*dx+dy*dy <= radius*radius
  }

}

object Tower {

	private final val FieldSeparator = ','
	private final val TrueChar = '1'
	private final val FalseChar = '0'

	private final val MeanEarthRadius = 6371000.785 //in meters
	private final val DistancePerDegree = 2*Pi*MeanEarthRadius/360 //of latitude and longitude at equator, in meters
	private final val DegreeInRadian = Pi/180 //conversion factor

	private final val DefaultRange = 10000 //meters

	/**
	 * @param mcc mobile country code, for example 260 for Poland.
	 * @param lon longitude in degrees between -180.0 and 180.0
	 * @param lat latitude in degrees between -90.0 and 90.0
	 * @param range estimate of cell range, in meters (optional)
	 * @param changeable defines if coordinates of the cell tower are exact or approximate
	 * @return a new tower instance
	 */
	def apply (mcc: Int, lon: Double, lat: Double, range: Option[Int], changeable: Boolean): Tower =
			new Tower(mcc, lon, lat, range, changeable)

	/**
	 * This is the reverse operation of `toString`: `Tower(t.toString)==t` for all
	 * towers `t`. The opposite is not always true, since whitespace might have
	 * been trimmed.
	 *
	 * @param string
	 * @return a new tower instance built from the specified string
	 * (comma-separated field values in the order: mcc, lon, lat, range,
	 * changeable (range can be empty and changeable is represented by '0' or '1')
	 * @throws IllegalArgumentException if string has wrong format
	 */
	def apply (string: String): Tower = try {
		val fields = string.split(FieldSeparator).map(_.trim())
		val mcc = fields(0).toInt
		val lon = fields(1).toDouble
		val lat = fields(2).toDouble
		val range = if (fields(3).isEmpty) None else Some(fields(3).toInt)
		val changeable = fields(4).toInt!=0
		Tower(mcc, lon, lat, range, changeable)
	} catch {
	  case t: Throwable => throw new IllegalArgumentException("cannot build tower", t)
	}

}
