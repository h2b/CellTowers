/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.cli

import java.io.File

import scala.io.Source
import scala.util.Try

import com.beust.jcommander.{ JCommander, Parameter }

import de.h2b.exercise.celltowers.io.Loader

/**
 * Checks if a point specified by latitude and longitude coordinates lies in the
 * range of one or more cell towers read from a CSV file in the format of
 * [[de.h2b.exercise.celltowers.io.FileWriter]].
 *
 * @author h2b
 */
object CoverageTester extends App {

  private object Options {

    @Parameter(description="inputfile", arity=1, required=true)
    var parameters: java.util.List[String] = _

    @Parameter(names=Array("--lat"), description="latitude of point to check (in degrees)", required=true)
    var lat: Double = _

    @Parameter(names=Array("--lon"), description="longitude of point to check (in degrees)", required=true)
    var lon: Double = _

    @Parameter(names=Array("-h", "--help"), description="show this help", help=true)
    var help: Boolean = _

  }

  {
    val commander = Try(new JCommander(Options, args: _*))
	  if (commander.isFailure) {
		  Console.err.println("command-line parsing error:")
		  Console.err.println(commander)
		  Console.err.println("enter -h or --help for usage hints")
		  sys.exit(1)
	  }
	  if (Options.help) {
		  commander.get.usage()
		  sys.exit(0)
	  }
	  val start = System.currentTimeMillis
	  Console.out.println("running...")
	  run()
	  val stop = System.currentTimeMillis
	  Console.out.println("runtime: " + (stop-start)/1000 + " s")
  }

  private def run () = try {
	  val in = new File(Options.parameters.get(0))
	  val source = Source.fromFile(in)
	  val loader = Loader.fromSource(source)
	  var counter = 0
	  for (tower ← loader if tower.isDefined && tower.get.isCovering(Options.lat, Options.lon)) {
		  Console.out.println(tower.get)
		  counter += 1
	  }
	  Console.out.println(counter + " tower(s) found")
	  source.close()
  } catch {
    case e: Throwable =>
      Console.err.println("runtime error: " + e.getLocalizedMessage())
      Console.err.println("enter -h or --help for usage hints")
  }

}