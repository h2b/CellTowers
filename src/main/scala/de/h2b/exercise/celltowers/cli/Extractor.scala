/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.cli

import java.io.{ File, IOException }

import scala.io.Source
import scala.util.Try

import com.beust.jcommander.{ JCommander, Parameter }

import de.h2b.exercise.celltowers.io.{ RawReader, Writer }

/**
 * Provides a command-line interface for extracting raw cell-tower data for a
 * specified mobile country code from an opencellid.org cell_towers.csv file.
 * The resulting data set is written to a new file using the format of
 * [[de.h2b.exercise.celltowers.io.FileWriter]] and can be further processed by
 * [[de.h2b.exercise.celltowers.io.Loader]].
 *
 * @author h2b
 */
object Extractor extends App {

  private object Options {

    @Parameter(description="infile outfile", arity=2, required=true)
    var parameters: java.util.List[String] = _

    @Parameter(names=Array("-m", "--mcc"), description="mobile country code", required=true)
    var mcc: Int = _

    @Parameter(names=Array("-a", "--append"), description="append output to outfile")
    var append: Boolean = _

    @Parameter(names=Array("-h", "--help"), description="show this help", help=true)
    var help: Boolean = _

  }

  {
	  val commander = Try(new JCommander(Options, args: _*))
	  if (commander.isFailure) {
		  Console.err.println("command-line parsing error:")
		  Console.err.println(commander)
		  Console.err.println("enter -h or --help for usage hints")
		  sys.exit(1)
	  }
	  if (Options.help) {
		  commander.get.usage()
		  sys.exit(0)
	  }
	  val start = System.currentTimeMillis
	  Console.out.println("running...")
	  run()
	  val stop = System.currentTimeMillis
	  Console.out.println("runtime: " + (stop-start)/1000 + " s")
  }

  private def run () = try {
	  val in = new File(Options.parameters.get(0))
	  val source = Source.fromFile(in)
	  val reader = RawReader(source)
	  val out = new File(Options.parameters.get(1))
	  val writer = Writer.toFile(out, Options.append)
	  for (tower ← reader if tower.isDefined && tower.get.mcc==Options.mcc)
	    writer.write(tower.get)
	  writer.close()
	  source.close()
	  Console.out.println("output written to " + out)
	  Console.out.println(reader.errorLineCount + " erroneous lines encountered")
  } catch {
    case e: Throwable =>
      Console.err.println("runtime error: " + e.getLocalizedMessage())
      Console.err.println("enter -h or --help for usage hints")
  }

}