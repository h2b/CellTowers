/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.io

import java.io.{ BufferedWriter, Closeable, File, FileWriter => JavaFileWriter, IOException }

import scala.util.Try

import de.h2b.exercise.celltowers.model.Tower

/**
 * A writer for `Tower` objects.
 *
 * @author h2b
 */
trait Writer extends Closeable {

	/**
	 * @param tower
	 * @throws IOException if an I/O error occurs
	 */
	@throws(classOf[IOException])
	def write (tower: Tower): Unit

}

object Writer {

	/**
	 * @param file
	 * @param append `true` if new content is to be appended to the existing one,
	 * `false` otherwise (defaults to `false`)
	 * @return a new writer for a tower-data file
	 * @throws IOException if the file exists but is a directory rather than a
	 * regular file, does not exist but cannot be created, or cannot be opened for
	 * any other reason
	 * @throws IllegalArgumentException if file is `null`
	 */
  @throws(classOf[IOException])
	def toFile (file: File, append: Boolean = false): Writer = new FileWriter(file, append)

}

class FileWriter private[io] (file: File, append: Boolean) extends Writer {

	require(file!=null, "file is null")

  private val out = new BufferedWriter(new JavaFileWriter(file))

  def write (tower: Tower): Unit = {
    out.write(tower.toString)
	  out.newLine()
  }

  def close (): Unit = Try(out.close())

}
