/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.io

import java.io.Closeable

import scala.io.Source
import scala.util.Try

import de.h2b.exercise.celltowers.model.Tower

/**
 * A loader for `Tower` objects provided by a source in `Tower.apply(String)`
 * format.
 *
 * Provides an iterator that in each next step tries to convert the next input
 * line into a `Tower` object and yields an `Option[Tower]` as the result.
 *
 * @author h2b
 */
trait Loader extends Iterable[Option[Tower]] with Closeable {

  var errorCount = 0L

}

object Loader {

	/**
	 * @param source
	 * @return an iterable providing an iterator over the towers read from source
	 * @note The source must provide lines `l` conforming to `Tower(l: String)`.
	 * @throws IllegalArgumentException if source is `null`
	 */
	def fromSource (source: Source): Loader = new FileLoader(source)

}

class FileLoader private[io] (source: Source) extends Loader {

	require(source!=null, "source is null")

  def iterator: Iterator[Option[Tower]] = new Iterator[Option[Tower]] {

	  private val lineIterator = source.getLines()

	  def hasNext = lineIterator.hasNext

	  def next () = {
	    if (!hasNext) throw new NoSuchElementException("iterator overflow")
      val line = lineIterator.next()
      val tower = Try(Tower(line))
      if (tower.isFailure) errorCount += 1
      tower.toOption
	  }

	}

  def close (): Unit = Try(source.close())

}
