/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.io

import scala.io.Source

import de.h2b.exercise.celltowers.model.Tower

/**
 * A reader for raw tower data provided by a source in opencellid.org's
 * cell_towers.csv format.
 *
 * Provides an iterator that in each next step tries to convert the next input
 * line into a `Tower` object and yields an `Option[Tower]` as the result.
 *
 * @author h2b
 */
class RawReader private (source: Source) extends Iterable[Option[Tower]] {

  import RawReader._

	require(source!=null, "source is null")

	var errorLineCount = 0L

  def iterator: Iterator[Option[Tower]] = new Iterator[Option[Tower]] {

	  private val lineIterator = source.getLines().drop(HeaderLineCount)

    def hasNext = lineIterator.hasNext

    def next () = {
	    if (!hasNext) throw new NoSuchElementException("iterator overflow")
      val line = lineIterator.next()
      tower(line)
    }

    private def tower (line: String): Option[Tower] = try {
      val fields = line.split(FieldSeparator).map(_.trim())
			val mcc = fields(FieldIndexMcc).toInt
			val lon = fields(FieldIndexLon).toDouble
			val lat = fields(FieldIndexLat).toDouble
			val range = if (fields(FieldIndexRange).isEmpty()) None else Some(fields(FieldIndexRange).toInt)
			val changeable = fields(FieldIndexChangeable).toInt!=0
			Some(Tower(mcc, lon, lat, range, changeable))
    } catch {
      case _: Throwable =>
        errorLineCount += 1
        None
    }

  }

}

object RawReader {

  private final val HeaderLineCount = 1
  private final val FieldSeparator = ','
  private final val FieldIndexMcc = 1
  private final val FieldIndexLon = 6
  private final val FieldIndexLat = 7
  private final val FieldIndexRange = 8
  private final val FieldIndexChangeable = 10

  /**
   * @param source
   * @return an iterable providing an iterator over the cell towers read from source
   * @note The iterator actually yields `Option[Tower]` elements which are
   * `None` if the corresponding source line cannot be converted.
	 * @throws IllegalArgumentException if source is `null`
   */
  def apply (source: Source): RawReader = new RawReader(source)

}