/*
  CellTowers -- Exercises on Extracting, Viewing and Coverage-Testing of Mobile Cell Towers

  Copyright 2015-16 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/

package de.h2b.exercise.celltowers.view

import scala.beans.BeanProperty
import scala.io.Source
import scala.sys.SystemProperties
import scala.util.Try

import org.primefaces.model.map.{ DefaultMapModel, LatLng, Marker }

import de.h2b.exercise.celltowers.io.Loader
import javax.annotation.PostConstruct
import javax.enterprise.context.RequestScoped
import javax.faces.application.FacesMessage
import javax.faces.application.FacesMessage.Severity
import javax.faces.context.FacesContext
import javax.inject.Named

/**
 * A bean that supports the index.xhtml web page.
 *
 * The tower data are read from a file towers.csv in the JBoss data directory.
 *
 * @note For testing purposes this bean is set to request-scoped. For production
 * purposes it can be set to session-scoped.
 * @author h2b
 */
@Named
@RequestScoped
@SerialVersionUID(1L)
class IndexBean extends Serializable {

  @BeanProperty
  val header = "Cell Towers"

  @BeanProperty
  val width = "100%"

  @BeanProperty
  val height = "800px"

  @BeanProperty
  val zoom = 6 //suitable for Egypt, should be computed somehow

  @BeanProperty
  var lon = 0.0

  @BeanProperty
  var lat = 0.0

  @BeanProperty
  val simpleModel = new DefaultMapModel()

  /**
   * Loads the towers, builds the simple model with marker overlays from the
   * tower coordinates and finds the appropriate positioning of the map view.
   */
  @PostConstruct
  def init (): Unit = {
    var latMin, lonMin = Double.MaxValue
    var latMax, lonMax = Double.MinValue
    val loader = Try(IndexBean.loader())
    if (loader.isFailure) {
      error("cannot load data source: " + loader)
      return
    }
    for (opt ← loader.get if opt.isDefined) {
      val tower = opt.get
      val coords = new LatLng(tower.lat, tower.lon)
      simpleModel.addOverlay(new Marker(coords))
      if (tower.lat<latMin) latMin = tower.lat
      if (tower.lat>latMax) latMax = tower.lat
      if (tower.lon<lonMin) lonMin = tower.lon
      if (tower.lon>lonMax) lonMax = tower.lon
    }
    loader.get.close()
    lon = lonMin+(lonMax-lonMin)/2
    lat = latMin+(latMax-latMin)/2
    val errorCount = loader.get.errorCount
    if (errorCount>0) warn(s"warning: data source is corrupt ($errorCount records skipped)")
  }

  private def error (msg: String) = log(FacesMessage.SEVERITY_ERROR, "Error!", msg)

  private def warn (msg: String) = log(FacesMessage.SEVERITY_WARN, "Warning!", msg)

  private def log (severity: Severity, summary: String, msg: String) = {
    println(msg)
    val context = FacesContext.getCurrentInstance
    context.addMessage(null, new FacesMessage(severity, summary, msg))
  }

}

object IndexBean {

  private val props = new SystemProperties()
  private val datadir = props.getOrElse("jboss.server.data.dir", "~")
  private val inputFilename = datadir + "/towers.csv"

  //might rise I/O exception
  private def loader (): Loader = Loader.fromSource(Source.fromFile(inputFilename))

}